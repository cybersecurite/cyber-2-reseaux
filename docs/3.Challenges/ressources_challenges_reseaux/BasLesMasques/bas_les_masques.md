---
hide:
  - toc
author: à compléter
title: Bas les masques !
---

# Challenge : bas les masques !

![](../masque_venitien.png){: .center }

Dans un réseau local, le masque de sous-réseau conditionne le nombre de machines qui peuvent y être connectés.

Par exemple, dans le réseau local `192.168.17.0` avec un sous-masque valant `255.255.255.0`, il ne peut y avoir que 
254 machines connectées.

!!! note "Votre objectif"
    Pour chacune des cinq situations suivantes, déterminer la dernière adresse IP disponible pour une machine.

    Le flag est cette adresse IP au format décimal pointé (par exemple, `192.168.17.254` avec les valeurs ci-dessus).

??? abstract "Indice 1"
    <a href='../calculs_sur_les_adresses_IP.png' target='_blank'>Documentation sur la manipulation des adresses IP</a>

??? abstract "Indice 2"
    <a href='https://fr.wikipedia.org/wiki/Sous-r%C3%A9seau' target='_blank'>Documentation sur les sous-réseaux</a>

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Situation 1</h2>
Informations disponibles sur le réseau local :
<ul>
    <li><strong>Adresse du réseau :</strong><code>155.18.10.0</code></li>
    <li><strong>Sous-masque :</strong><code>255.255.255.0</code></li>
</ul>
<br>
<p>
    <form id="form_bas_les_masques1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>

<div id='Partie2' hidden>
<br>
<h2>Situation 2</h2>
Informations disponibles sur le réseau local :
<ul>
    <li><strong>Adresse du réseau :</strong><code>137.117.25.0/24</code></li>
</ul>
<br>
<p>
    <form id="form_bas_les_masques2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>

<div id='Partie3' hidden>
<br>
<h2>Situation 3</h2>
Informations disponibles sur le réseau local :
<ul>
    <li><strong>Adresse du réseau :</strong><code>145.110.0.0/16</code></li>
</ul>
<br>
<p>
    <form id="form_bas_les_masques3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>

<div id='Partie4' hidden>
<br>
<h2>Situation 4</h2>
Informations disponibles sur le réseau local :
<ul>
    <li><strong>Adresse du réseau :</strong><code>166.102.192.0/18</code></li>
</ul>
<br>
<p>
    <form id="form_bas_les_masques4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>

<div id='Partie5' hidden>
<br>
<h2>Situation 5</h2>
Informations disponibles sur le réseau local :
<ul>
    <li><strong>Adresse du réseau :</strong><code>166.102.192.0/19</code></li>
</ul>
<br>
<p>
    <form id="form_bas_les_masques5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>


<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Les calculs sur les adresses IP n'ont aucun secret pour vous !</p>

!!! danger "A retenir"

    Toutes les machines connectées à Internet sont repérées par une adresse IP qui contient à la fois l'adresse du réseau local et l'adresse de l'hôte dans ce réseau. Une bonne connaissance de la manipulation de ces adresses est fondamentale pour comprendre comment les paquets sont acheminées entre deux machines et permet, dans le cadre de la cybersécurité, de tracer des attaques pour pouvoir remonter à leurs sources.

</div>

<script src='../script_chall_bas_les_masques.js'></script>