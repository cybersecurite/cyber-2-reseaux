---
hide:
  - toc
author: à compléter
title: Réseau local
---

# Challenge : réseau local

![](../reseau_local.png){: .center }

Une attaque émanant de trois machines situées dans un même réseau local et visant une infrastructure sensible dont 
vous êtes le chef de la sécurité informatique vient d'être détectée. L'équipe sous vos ordres a pu déterminer rapidement 
six adresses IP potentielles parmi lesquelles se trouvent les trois machines à l'origine de l'attaque :

 - **Machine 1 :** `129.175.127.1/22`
 - **Machine 2 :** `129.175.13.10/22`
 - **Machine 3 :** `129.175.128.7/22`
 - **Machine 4 :** `129.175.131.110/22`
 - **Machine 5 :** `129.175.132.58/22`
 - **Machine 6 :** `129.175.129.12/22`


!!! note "Votre objectif"
    Identifier les trois machines à l'origine de l'attaque.

    Le flag est la concaténation, sans caractères spéciaux, des trois parties hôtes de ces machines, prises dans l'ordre croissant de leur numéro.

    Par exemple, si les trois parties hôtes sont `0.0.8.15`, `0.0.100.45` et `0.1.12.0`, alors le flag est 
    `00815001004501120`


??? abstract "Indice"
    <a href='../calculs_sur_les_adresses_IP.png' target='_blank'>Documentation sur la manipulation des adresses IP</a>

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<br>
<p>
    <form id="form_reseau_local1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez identifié les trois machines et avez pu ainsi faire cesser l'attaque !<br><br>
Les calculs sur les adresses IP n'ont aucun secret pour vous !</p>

!!! danger "A retenir"

    Toutes les machines connectées à Internet sont repérées par une adresse IP qui contient à la fois l'adresse du réseau local et l'adresse de l'hôte dans ce réseau. Une bonne connaissance de la manipulation de ces adresses est fondamentale pour comprendre comment les paquets sont acheminées entre deux machines et permet, dans le cadre de la cybersécurité, de tracer des attaques pour pouvoir remonter à leurs sources.

</div>

<script src='../script_chall_reseau_local.js'></script>